import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

first_row = ["news", "type"]

data = pd.read_csv("C:/Users/DD/Desktop/Projects/dataset.csv.txt",
                   sep=",", names=first_row, encoding="latin-1")

data_set = np.asarray(data)

data_news = data_set[:, 0]
data_types = data_set[:, 1]

types = {}
for i in range(data_types.shape[0]):
    if types.get(data_types[i], 0) == 0:
        types.update({data_types[i]: 1})
    else:
        types.update({data_types[i]: types.get(data_types[i]) + 1})


def LR(N):
    # Tạo các data
    data_train = []
    label_train = []
    test_data = []
    test_labels = []
    # 300 bài báo mỗi loại
    countB = 0
    countE = 0
    countP = 0
    countS = 0
    countT = 0

    for i in range(data_news.shape[0]):
        if countB < N and data_types[i] == "business":
            data_train.append(data_news[i])
            label_train.append("business")
            countB += 1
        elif countB < types.get("business") and data_types[i] == "business":
            test_data.append(data_news[i])
            test_labels.append("business")
            countB += 1
        if countE < N and data_types[i] == "entertainment":
            data_train.append(data_news[i])
            label_train.append("entertainment")
            countE += 1
        elif countB < types.get("entertainment") and data_types[i] == "entertainment":
            test_data.append(data_news[i])
            test_labels.append("entertainment")
            countB += 1
        if countP < N and data_types[i] == "politics":
            data_train.append(data_news[i])
            label_train.append("politics")
            countP += 1
        elif countB < types.get("politics") and data_types[i] == "politics":
            test_data.append(data_news[i])
            test_labels.append("politics")
            countB += 1
        if countS < N and data_types[i] == "sport":
            data_train.append(data_news[i])
            label_train.append("sport")
            countS += 1
        elif countB < types.get("sport") and data_types[i] == "sport":
            test_data.append(data_news[i])
            test_labels.append("sport")
            countB += 1
        if countT < N and data_types[i] == "tech":
            data_train.append(data_news[i])
            label_train.append("tech")
            countT += 1
        elif countB < types.get("tech") and data_types[i] == "tech":
            test_data.append(data_news[i])
            test_labels.append("tech")
            countB += 1

    count_vector = CountVectorizer(stop_words = 'english')
    training_data = count_vector.fit_transform(data_train)
    testing_data = count_vector.transform(test_data)

    clf = LogisticRegression(random_state=0).fit(training_data, label_train)

    return N, clf.score(testing_data, test_labels)

maxScore = 0
num_trained = 0
for i in range(1, 387):
    num_trained_tmp, max_tmp = LR(i)
    if max_tmp > maxScore:
        num_trained = num_trained_tmp
        maxScore = max_tmp
print("Độ chính xác tối đa đath được: " + str(maxScore * 100))
print("Số train cần: " + str(num_trained))
# model tốt nhất 368 train