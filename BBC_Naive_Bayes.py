import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

first_row = ["news", "type"]

data = pd.read_csv("C:/Users/DD/Desktop/Projects/dataset.csv.txt", sep = ",", names=first_row, encoding="latin-1")

data_set = np.asarray(data)

data_news = data_set[:, 0]
data_types = data_set[:, 1]

types = {}
for i in range(data_types.shape[0]):
    if types.get(data_types[i], 0) == 0:
        types.update({data_types[i] : 1})
    else:
        types.update({data_types[i] : types.get(data_types[i]) + 1})

# types = {'business': 510, 'entertainment': 386, 'politics': 417, 'sport': 511, 'tech': 401} --> 5 loại thông tin
def naiveBayes(N):
    # Tạo các data
    data_train = []
    label_train = []
    test_data = []
    test_labels = []
    # 300 bài báo mỗi loại 
    countB = 0
    countE = 0
    countP = 0
    countS = 0
    countT = 0

    for i in range(data_news.shape[0]):
        if countB < N and data_types[i] == "business":
            data_train.append(data_news[i])
            label_train.append("business")
            countB += 1
        elif countB < types.get("business") and data_types[i] == "business":
            test_data.append(data_news[i])
            test_labels.append("business")
            countB += 1
        if countE < N and data_types[i] == "entertainment":
            data_train.append(data_news[i])
            label_train.append("entertainment")
            countE += 1
        elif countB < types.get("entertainment") and data_types[i] == "entertainment":
            test_data.append(data_news[i])
            test_labels.append("entertainment")
            countB += 1
        if countP < N and data_types[i] == "politics":
            data_train.append(data_news[i])
            label_train.append("politics")
            countP += 1
        elif countB < types.get("politics") and data_types[i] == "politics":
            test_data.append(data_news[i])
            test_labels.append("politics")
            countB += 1
        if countS < N and data_types[i] == "sport":
            data_train.append(data_news[i])
            label_train.append("sport")
            countS += 1
        elif countB < types.get("sport") and data_types[i] == "sport":
            test_data.append(data_news[i])
            test_labels.append("sport")
            countB += 1
        if countT < N and data_types[i] == "tech":
            data_train.append(data_news[i])
            label_train.append("tech")
            countT += 1
        elif countB < types.get("tech") and data_types[i] == "tech":
            test_data.append(data_news[i])
            test_labels.append("tech")
            countB += 1

    count_vector = CountVectorizer(stop_words = 'english')
    training_data = count_vector.fit_transform(data_train)
    testing_data = count_vector.transform([news for news in test_data])

    naive_bayes = MultinomialNB()
    naive_bayes.fit(training_data, label_train)

    predictions = naive_bayes.predict(testing_data)

    from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score

    return (N, accuracy_score(test_labels, predictions))

maxAcc = 0
train_num = 0
for i in range(1, 387):
    number_of_train, acc = naiveBayes(i)
    if acc> maxAcc:
        maxAcc = acc
        train_num = number_of_train
print("Điểm chính xác tối đa " + str(maxAcc * 100))
# số data cần lấy để train ra tốt nhất là 181




