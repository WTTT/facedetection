import numpy as np
import cv2
import MyLib as mlib
import os
import random as random

faces, facesID = mlib.labels_for_training("C:/Users/DD/Desktop/Projects/Training")
face_recog = mlib.train_classifier(faces, facesID)
name = { 0 : "Giang Nam", 1 : "D.Trump", 2 : "Unknown", 3 : "Khai Nguyen"}

prototxt = "C:/Users/DD/Desktop/Projects/deploy.prototxt.txt"
model = "C:/Users/DD/Desktop/Projects/res10_300x300_ssd_iter_140000.caffemodel"

# chạy model caffe
net = cv2.dnn.readNetFromCaffe(prototxt, model)

def caffeModelForPic(directory = "C:/Users/DD/Desktop/Projects/Test/test_19.jpg"):
    image = cv2.imread(directory)
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

    net.setInput(blob)
    detections = net.forward()

    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.15:
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            text = "{:.2f}%".format(confidence * 100)
            if startY - 10 > 10:
                y = startY - 10
            else:
                y = startY + 10
            cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0 ,255), 2)
            cv2.putText(image, text, (startX, y), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 255, 0), 1)


    cv2.imshow("Result", image)
    cv2.waitKey(0)

def caffeModelForCam(img, num):
    num = 0
    (h, w) = img.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

    net.setInput(blob)
    detections = net.forward()

    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.90:
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            if startY - 10 > 10:
                y = startY - 10
            else:
                y = startY + 10
            sub_img = img[startX:endX, startY:endY]
            try:
                cv2.imwrite("C:/Users/DD/Desktop/Projects/Store/Khai_0.jpg", sub_img)
                roi_gray = cv2.cvtColor(sub_img, cv2.COLOR_RGB2GRAY)
                labels, confidence = face_recog.predict(roi_gray)
                cv2.rectangle(img, (startX, startY), (endX, endY), (0, 0 ,255), 2)
                if confidence < 85:
                    cv2.putText(img, name.get(labels), (startX, y), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 255, 0), 1)
                else:
                    cv2.putText(img, "New Face", (startX, y), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 255, 0), 1)
            except:
                cv2.error