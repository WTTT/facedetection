import cv2 

# Lọc lần 1
face_cascade = cv2.CascadeClassifier('C:/Users/DD/Desktop/Projects/haar-cascade-files-master/haar-cascade-files-master/haarcascade_frontalface_default.xml')

number = int(input("Enter a number of pictures: "))
result_list = []

test_path = 'C:/Users/DD/Desktop/Projects/Test/test_'
type_img = ['.jfif', '.jpg', '.png']

for i in range(number):
    for k in range(len(type_img)):
        img_path = test_path + str(i) + type_img[k]
        img = cv2.imread(img_path)
        if hasattr(img, 'shape'):
            gray_scale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            if gray_scale.shape[0] == 0:
                continue
            faces = face_cascade.detectMultiScale(gray_scale, 1.1, 5)
            for (x, y, w, h) in faces:
                imgResult = img[y:(y+h), x:(x + w)]
                result_list.append((imgResult, k))
                # cv2.imshow('image', imgResult)
                # cv2.waitKey()
            break
        else: 
            continue

result_path = 'C:/Users/DD/Desktop/Projects/Result/reuslt_'
for i in range(len(result_list)):
    cv2.imwrite(result_path + str(i) + ".jpg", result_list[i][0])

print("Number of detected faces: " + str(len(result_list)))

# Lọc lần hai
import masterLib as msL
import os
files = os.walk("C:/Users/DD/Desktop/Projects/Result")
files = list(files)
root = files[0][0]
files = files[0][2]
n_not_found = 0
n_trained = 0
for filename in files:
    try:
        n1_trained, n1_not_found = msL.masterLib("C:/Users/DD/Desktop/Projects/Result/" + str(filename), n_trained, n_not_found)
        if n1_trained > n_trained: 
            n_trained += 1
        if n1_not_found > n_not_found:
            n_not_found += 1
    except:
        cv2.error
