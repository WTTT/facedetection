import os
import numpy as np
import cv2

# STATIC 
face_cascade = cv2.CascadeClassifier('C:/Users/DD/Desktop/Projects/haar-cascade-files-master/haar-cascade-files-master/haarcascade_frontalface_default.xml')

def faceDetection(img):
    gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    faces_detected = face_cascade.detectMultiScale(gray_img, scaleFactor=1.1, minNeighbors=4)
    return faces_detected, gray_img  

def labels_for_training(directory):
    faces = []
    facesID = []
    for (root, dirs, files) in os.walk(directory):
        for file_name in files:
            if file_name.startswith("."):
                # Nothing to read
                continue
            else:
                id = os.path.basename(root)
                img_path = os.path.join(directory, id, file_name)
                img = cv2.imread(img_path)
                (faces_detected, gray_img) = faceDetection(img)
                if len(faces_detected) > 1:
                    # only one face is recognized
                    continue 
                elif len(faces_detected) == 1:
                    (x, y, w, h) = faces_detected[0]
                    roi_gray = gray_img[y:(y+w), x:(x+h)]
                    faces.append(roi_gray)
                    facesID.append(int(id))
        
    return (faces, facesID)


def train_classifier(faces, facesID):
    face_recog = cv2.face.LBPHFaceRecognizer_create()
    face_recog.train(faces, np.asarray(facesID))
    return face_recog

def draw_rect_around(img, faces):
    (x, y, w, h) = faces
    cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), thickness=1)

def putText(img, text, x, y):
    cv2.putText(img, text, (x, y+20), cv2.FONT_HERSHEY_DUPLEX, 0.75, (255, 0, 0), 2)

