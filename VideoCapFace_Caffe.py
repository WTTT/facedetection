import cv2 
import MyLib as MyLib
import CaffeModel as caffe

vid = cv2.VideoCapture(0) 
num = 0
while(True): 
    ret, frame = vid.read()
    caffe.caffeModelForCam(frame, num)
    cv2.imshow("Frame", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'): 
        break
  
vid.release() 
cv2.destroyAllWindows() 