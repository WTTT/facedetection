
import cv2 
import MyLib as MyLib

vid = cv2.VideoCapture(0) 
face_cascade = cv2.CascadeClassifier('C:/Users/DD/Desktop/Projects/haar-cascade-files-master/haar-cascade-files-master/haarcascade_frontalface_default.xml')
faces, facesID = MyLib.labels_for_training("C:/Users/DD/Desktop/Projects/Training")
face_recog = MyLib.train_classifier(faces, facesID)
names = {0:"Giang Nam", 1:"D.Trump", 2:"Unknown"}
while(True): 
    ret, frame = vid.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 5)
    for face in faces:
        (x, y, w, h) = face
        roi_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)[y:y+h, x:x+w]
        labels, confidence = face_recog.predict(roi_gray)
        if confidence < 39:
            try:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0 , 0), 2)
                cv2.putText(frame, names.get(labels), (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0), 2)
            except:
                SystemError
        elif confidence < 100:
            try:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0 , 0), 2)
                cv2.putText(frame, "New Face", (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0), 2)
            except:
                SystemError
                print("Error")
    cv2.imshow("Frame", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'): 
        break
  

vid.release() 
cv2.destroyAllWindows() 