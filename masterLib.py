import MyLib as mlib
import os
import cv2
def masterLib(directory, n_trained, n_not_found):
    faces, facesID = mlib.labels_for_training("C:/Users/DD/Desktop/Projects/Training")
    face_recog = mlib.train_classifier(faces, facesID)

    name = { 0 : "Giang Nam", 1 : "D.Trump", 2 : "Unknown", 3 : "Khai Nguyen"}

    img = cv2.imread(directory)

    face_detected, gray_img = mlib.faceDetection(img)

    if len(face_detected) == 0:
        print(directory + ": No human!")
    else:
        for face in face_detected:
            (x, y, w, h) = face
            roi_gray = gray_img[y:(y+h), x:(w+x)]
            labels, confidence = face_recog.predict(roi_gray)
            mlib.draw_rect_around(img, face)
            if confidence < 87:
                mlib.putText(img, name.get(labels), x, y)
                # cv2.imwrite("C:/Users/DD/Desktop/Projects/Final_Results/" + name.get(labels) + "_" + str(n_trained) +  ".jpg", img)
                n_trained += 1
            else:
                mlib.putText(img, "Not Found", x, y)
                # cv2.imwrite("C:/Users/DD/Desktop/Projects/Final_Results/Not_found_" + str(n_not_found) + ".jpg", img)
                n_not_found += 1
        cv2.imshow("Result", img)
        cv2.waitKey()
    return (n_trained, n_not_found)

def masterLib1(img):
    faces, facesID = mlib.labels_for_training("C:/Users/DD/Desktop/Projects/Training")
    face_recog = mlib.train_classifier(faces, facesID)

    name = { 0 : "Giang Nam", 1 : "D.Trump", 2 : "Unknown"}

    face_detected, gray_img = mlib.faceDetection(img)

    if len(face_detected) == 0:
        pass
    else:
        for face in face_detected:
            (x, y, w, h) = face
            roi_gray = gray_img[y:(y+h), x:(w+x)]
            labels, confidence = face_recog.predict(roi_gray)
            mlib.draw_rect_around(img, face)
            if confidence < 87:
                mlib.putText(img, name.get(labels), x, y)
            else:
                mlib.putText(img, "Not Found", x, y)
    return img

