import numpy as np
import cv2
import MyLib as mlib
import os

prototxt = "C:/Users/DD/Desktop/Projects/deploy.prototxt.txt"
model = "C:/Users/DD/Desktop/Projects/res10_300x300_ssd_iter_140000.caffemodel"

# chạy model caffe
net = cv2.dnn.readNetFromCaffe(prototxt, model)

image = cv2.imread("C:/Users/DD/Desktop/Projects/Test/test_19.jpg")
(h, w) = image.shape[:2]
blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

net.setInput(blob)
detections = net.forward()

face_list = []
for i in range(detections.shape[2]):
    confidence = detections[0, 0, i, 2]
    if confidence > 0.15:
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        text = "{:.2f}%".format(confidence * 100)
        if startY - 10 > 10:
            y = startY - 10
        else:
            y = startY + 10
        cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0 ,255), 2)
        cv2.putText(image, text, (startX, y), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 255, 0), 1)


cv2.imshow("Result", image)
cv2.waitKey(0)

