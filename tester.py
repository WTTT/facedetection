import MyLib as mlib
import os
import cv2
def masterLib(num):
    faces, facesID = mlib.labels_for_training("C:/Users/DD/Desktop/Projects/Training")
    face_recog = mlib.train_classifier(faces, facesID)

    name = { 0 : "Giang Nam", 1 : "D.Trump", 2 : "Unknown"}

    img = cv2.imread("C:/Users/DD/Desktop/Projects/Result/reuslt_" + str(num) + ".jpg")

    face_detected, gray_img = mlib.faceDetection(img)

    if len(face_detected) == 0:
        print("No human!")
        os.remove("C:/Users/DD/Desktop/Projects/Result/reuslt_" + str(num) + ".jpg")
    else:
        for face in face_detected:
            (x, y, w, h) = face
            roi_gray = gray_img[y:(y+h), x:(w+x)]
            labels, confidence = face_recog.predict(roi_gray)
            mlib.draw_rect_around(img, face)
            if confidence < 87:
                mlib.putText(img, name.get(labels), x, y)
            else:
                mlib.putText(img, "Not Found", x, y)
        # cv2.imshow("Result", img)
        # cv2.waitKey()

for i in range(185):
    masterLib(i)
